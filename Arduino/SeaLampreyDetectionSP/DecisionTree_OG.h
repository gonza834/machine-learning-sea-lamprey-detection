#pragma once

namespace Eloquent3 {
    namespace ML {
        namespace Port {
            class DecisionTree {
                public:
                    /**
                    * Predict class for features vector
                    */
                    int predict(float *x) {
                        if (x[0] <= 0.8163333237171173) {
                            if (x[0] <= 0.7970000207424164) {
                                if (x[1] <= 0.7092393934726715) {
                                    if (x[1] <= 0.2740214467048645) {
                                        return 1;
                                    }

                                    else {
                                        if (x[0] <= 0.31466667354106903) {
                                            return 1;
                                        }

                                        else {
                                            return 0;
                                        }
                                    }
                                }

                                else {
                                    return 1;
                                }
                            }

                            else {
                                if (x[1] <= 0.491630420088768) {
                                    if (x[0] <= 0.8143333494663239) {
                                        return 0;
                                    }

                                    else {
                                        return 0;
                                    }
                                }

                                else {
                                    if (x[1] <= 0.6255436539649963) {
                                        if (x[0] <= 0.8036666810512543) {
                                            if (x[1] <= 0.5083695650100708) {
                                                return 0;
                                            }

                                            else {
                                                return 1;
                                            }
                                        }

                                        else {
                                            if (x[0] <= 0.8149999976158142) {
                                                return 0;
                                            }

                                            else {
                                                return 1;
                                            }
                                        }
                                    }

                                    else {
                                        return 1;
                                    }
                                }
                            }
                        }

                        else {
                            if (x[0] <= 0.8221111297607422) {
                                if (x[1] <= 0.491630420088768) {
                                    if (x[0] <= 0.8210000097751617) {
                                        return 1;
                                    }

                                    else {
                                        return 1;
                                    }
                                }

                                else {
                                    return 1;
                                }
                            }

                            else {
                                if (x[1] <= 0.491630420088768) {
                                    if (x[0] <= 0.8536666631698608) {
                                        if (x[1] <= 0.4748912751674652) {
                                            return 1;
                                        }

                                        else {
                                            if (x[0] <= 0.8236666619777679) {
                                                return 1;
                                            }

                                            else {
                                                return 1;
                                            }
                                        }
                                    }

                                    else {
                                        return 1;
                                    }
                                }

                                else {
                                    if (x[0] <= 0.8383333384990692) {
                                        return 1;
                                    }

                                    else {
                                        return 1;
                                    }
                                }
                            }
                        }
                    }

                protected:
                };
            }
        }
    }
