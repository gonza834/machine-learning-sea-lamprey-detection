#pragma once
#define _USE_MATH_DEFINES // for C
#include <math.h>
namespace Eloquent1 {
    namespace ML {
        namespace Port {
            class GaussianNB {
                public:
                    /**
                    * Predict class for features vector
                    */
                    int predict(float *x) {
                        float votes[2] = { 0.0f };
                        float theta[2] = { 0 };
                        float sigma[2] = { 0 };
                        theta[0] = 0.797201892872; theta[1] = 0.489291941288;
                        sigma[0] = 0.002103367503; sigma[1] = 0.002242049135;
                        votes[0] = log(0.323911836994) - gauss(x, theta, sigma);
                        theta[0] = 0.8295455422; theta[1] = 0.504739815306;
                        sigma[0] = 0.001155812649; sigma[1] = 0.025621262714;
                        votes[1] = log(0.676088163006) - gauss(x, theta, sigma);
                        // return argmax of votes
                        uint8_t classIdx = 0;
                        float maxVotes = votes[0];

                        for (uint8_t i = 1; i < 2; i++) {
                            if (votes[i] > maxVotes) {
                                classIdx = i;
                                maxVotes = votes[i];
                            }
                        }

                        return classIdx;
                    }

                protected:
                    /**
                    * Compute gaussian value
                    */
                    float gauss(float *x, float *theta, float *sigma) {
                        float gauss = 0.0f;

                        for (uint16_t i = 0; i < 2; i++) {
                            gauss += log(2 * M_PI * sigma[i]);
                            gauss += pow(double(x[i] - theta[i]), double(2)) / sigma[i];
                        }

                        return 0.5*gauss;
                    }
                };
            }
        }
    }
