#pragma once

namespace Eloquent4 {
    namespace ML {
        namespace Port {
            class SEFR {
                public:
                    /**
                    * Predict class for features vector
                    */
                    int predict(float *x) {
                        return dot(x,   0.02440368718  , 0.015276974091 ) <= 0.027060315149779916 ? 0 : 1;
                    }

                protected:
                    /**
                    * Compute dot product
                    */
                    float dot(float *x, ...) {
                        va_list w;
                        va_start(w, 2);
                        float dot = 0.0;

                        for (uint16_t i = 0; i < 2; i++) {
                            const float wi = va_arg(w, double);
                            dot += x[i] * wi;
                        }

                        return dot;
                    }
                };
            }
        }
    }
