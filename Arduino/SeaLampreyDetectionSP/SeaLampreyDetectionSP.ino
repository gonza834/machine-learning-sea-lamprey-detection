extern "C" {
#include "Tinn.h"
};
int ML_set = 1; //0 for original set, 1 for ref

//Selecting between OG and Ref trained models
#if ML_set
//loading the trained models
#include "GaussNB_ref.h"
#include "RandomForest_ref.h"
#include "DecisionTree_ref.h"
#include "SEFR_ref.h"

//Threshold values
double V1_bh = 0.82237967;
double V1_bl = 0.32421867;
double r1_bh = 1.22225504 ;
double r1_bl = 0.23528082;

double V2_bh = 0.79880256;
double V2_bl = 0.51726076;
double r2_bh = 0.70216792;
double r2_bl = 0.32990316;

double V3_bh = 0.81407758 ;
double V3_bl = 0.38311498 ;
double r3_bh = 0.64207279 ;
double r3_bl = 0.27848292 ;



//LGR Parameters
double inter = -28.26124355;
double slope[2] = {34.36839978,  2.34568535} ;

//NN model parameters for 6,12,24, and 36 hidden neurons
float biases6[] = {
  3.93166055,  5.03691269, -4.83590934, -3.58728863,  4.12589208,
  -4.5631099, 2.51051518
};

float weights6[] = {
  -5.17021946,  0.94734318, -4.7315258 , -2.20919222,  4.56343872,
  2.10121399,  5.04115818, -1.53188054, -5.77530819,  1.67950756,
  4.32036559,  1.9810355, -8.04734621, -8.83315606,  9.46163019,  8.9667038 , -9.54316797,
  8.50479
};


float biases12[] = {
  -3.09838084, -3.83044877,  3.98286276, -4.35383266, -4.04209218,
  -2.93450967,  3.69393698, -3.20805798,  4.03002774, -4.46811923,
  -3.10240359,  3.22677428, -0.23241338
};

float weights12[] = {
  4.07306518, -1.50673426,  3.62065868,  1.35176066, -3.92345662,
  -1.18046207,  3.63205046,  2.2505102 ,  3.48359523,  1.92155928,
  3.71606641, -1.28659699, -4.18467948, -0.00642879,  4.22329527,
  -1.52143872, -4.24297957, -0.75083221,  3.81703876,  2.19346801,
  4.076402  , -1.50364752, -4.25951798,  1.52970671, 5.05018919,
  4.69804047, -7.27885609,  5.8150157 ,  5.19929806, 4.11304543,
  -6.83669534,  5.47429568, -7.05988461,  5.82649257, 5.27408496,
  -6.51888957
};


float biases24[] = {
  2.38691696, -3.31937676,  2.58329922,  3.28799714, -2.28483458,
  2.60639749, -2.25743132, -2.97725628, -2.94769587, -3.07230194,
  -2.29621499,  3.17617628,  2.46395628, -2.98819473,  3.53310005,
  -2.85734966, -3.40552235, -2.76216963,  2.92528531, -2.81717587,
  3.56192086, -2.62865214,  2.77584168,  2.24397945, 1.19525699
};

float weights24[] = {
  -3.28502225,  1.31334426,  2.85453814,  1.7380009 , -3.52766045,
  1.36573012, -2.86880122, -1.67931131,  3.14315685, -1.2792783 ,
  -3.56103306,  1.37713367,  3.11549266, -1.27775708,  2.9431417 ,
  1.06166474,  2.53602899,  1.60722612,  3.05350151,  1.0522991 ,
  3.10918486, -1.20247555, -2.8816774 , -1.48811533, -3.38051069,
  1.33618911,  2.96384703,  1.04818129, -3.19982507, -1.61046331,
  2.87570781,  0.96119584,  2.73605511,  2.02676261,  3.03436923,
  0.46376317, -3.20429431, -0.50296416,  2.49046505,  1.47588796,
  -2.92228724, -2.02937077,  3.57965882, -1.3855756 , -3.77902428,
  1.42052824, -3.10392994,  1.25966169, -3.43927323,  4.00506788,
  -3.88288247, -3.51408264,  3.84561303, -3.83428879,  3.68650602,
  3.46706163,  3.38994012,  3.57847672, 3.5720461 , -3.11616791,
  -3.62353633,  3.62211423, -3.94203765, 3.46926734,  4.03138384,
  3.25117638, -3.29516482,  3.2306188 , -3.66752746,  4.3903864 ,
  -3.94745142, -3.11050348
};

float biases36[] = {
  -2.91205957, -2.00932004,  2.82179695,  2.17775782, -2.3707568 ,
  -2.75484029,  2.85196821, -2.39891688, -1.97916097, -2.04938114,
  -2.55497097,  2.35508141,  2.1898345 ,  2.95145704,  2.80601552,
  2.58634841, -2.01904111, -2.27631841, -2.88086705, -1.84161308,
  2.04848695,  2.82206689, -2.66413632, -2.76342952, -2.28406177,
  2.50363241, -2.90343405,  2.41282242, -2.05470486, -2.08517256,
  -1.7756766 , -2.45842475, -2.28409745,  2.20348906,  1.96030538,
  3.09134623, 0.28083899
};

float weights36[] = {
  2.45330851,  1.62878601,  2.70449616, -1.03246369, -2.69073504,
  -1.17098963, -2.93430061,  1.14935104,  2.04148022,  1.40401889,
  2.23053286,  1.69464217, -2.66454285, -1.25682154,  2.33265874,
  1.02266422,  2.69098092, -1.09094366,  2.76493894, -1.10416107,
  2.30861895,  1.2973987 , -2.50860727, -0.6619739 , -2.94056658,
  1.14084516, -2.2539977 , -1.95869328, -2.61873334, -1.25079443,
  -2.76559742, -0.63068135,  2.73387499, -1.10092157,  3.03766153,
  -1.18182461,  2.5021048 ,  1.51584984,  2.54199241, -1.04502582,
  -2.77444239,  1.08796066, -2.27441083, -1.74619002,  2.50424182,
  1.18190869,  2.48493557,  1.36305952,  3.05044244, -1.18792785,
  -2.34776697, -1.17998634,  2.25478034,  1.87752678, -3.21280165,
  1.20691248,  2.77161473, -1.10815678,  2.80543722, -1.11903281,
  2.45712138, -0.96758461,  2.5569013 ,  0.7497184 ,  3.03649691,
  -1.16372378, -2.95392557,  1.13802541, -2.68698267,  1.08003832,
  -2.57167711, -1.74950545, 2.91043443,  2.67538058, -3.23788885,
  -3.06345481,  2.25753404, 2.64419922, -3.20392005,  2.28385568,
  2.5726911 ,  2.77819271, 2.41595662, -2.53037965, -3.17004773,
  -3.11438501, -3.25403232, -2.68951732,  2.69731342,  2.96964052,
  2.7755372 ,  2.40126694, -2.71416142, -2.80268067,  2.52636011,
  2.86794874,  2.99676079, -2.56229332,  2.92995447, -3.39337275,
  2.71072864,  2.49267725, 2.26119541,  2.44451761,  2.95549877,
  -2.97627859, -2.7117691, -3.43246446
};
#else
#include "GaussNB_OG.h"
#include "RandomForest_OG.h"
#include "DecisionTree_OG.h"
#include "SEFR_OG.h"

//Threshold Values
double V1_bh = 0.82445912 ;
double V1_bl = 0.18825779 ;
double r1_bh = 1.49424131 ;
double r1_bl = 0.20001973 ;

double V2_bh = 0.79887421;
double V2_bl = 0.50991130;
double r2_bh = 0.66998729;
double r2_bl = 0.28373374;


double V3_bh = 0.81516723 ;
double V3_bl = 0.60770474 ;
double r3_bh = 0.64037777 ;
double r3_bl = 0.30989100 ;

//LGR Parameters
double inter = -25.12008986;
double slope[2] = {29.98614767,  2.57594401};

//NN model paremeters

float biases6[] = {
  3.95898784,  4.49654247, -4.29757954, -3.19097588,  3.68919692,
  -4.03101173, 2.05016567
};

float weights6[] = {
  -4.42845798, -0.76661149, -4.31771932, -1.91059315,  4.17691012,
  1.76389221,  4.58969638, -1.39591496, -5.26677275,  1.52908706,
  3.93151737,  1.65779439, -7.10526649, -7.48765251,  7.41975664,  7.41778655, -8.63346888,
  6.62259547
};

float biases12[] = {
  -2.6769851 , -3.35413208,  3.52633442, -3.87411412, -3.57822079,
  -2.58099117,  3.48878439, -2.80273245,  3.48125887, -3.97312595,
  -2.69673625,  2.814941, -0.32612811
};

float weights12[] = {
  3.62516201, -1.2606118 ,  3.29134099,  1.12554035, -3.5529447 ,
  -1.04525394,  3.21707947,  2.16535308,  3.10466389,  1.81027196,
  3.30976765, -0.99104755, -3.57959479, -0.92857118,  3.78588625,
  -1.27583173, -3.95764606, -0.19622825,  3.36281978,  2.13085012,
  3.64824912, -1.26037524, -3.81197161,  1.28018245, 3.84576698,
  3.74079296, -6.42840214,  4.58992761,  4.12454601, 3.13559549,
  -6.091439  ,  4.24875977, -6.21077371,  4.51877001, 4.10271553,
  -5.71397761
};

float biases24[] = {
  1.98312005, -2.88175042,  2.15664268,  2.84244578, -1.92478486,
  2.17834768, -1.88329751, -2.5696778 , -2.54249609, -2.67628071,
  -1.99948669,  2.73946715,  2.04615304, -2.57629816,  3.05382315,
  -2.46418324, -2.96171826, -2.46304447,  2.56835578, -2.43179829,
  3.06218365, -2.24371583,  2.34380234,  1.85642923, 0.80045659
};

float weights24[] = {
  -2.93196851,  1.04982778,  2.57791646,  1.5713712 , -3.11393754,
  1.08366797, -2.56150689, -1.54466636,  2.84187195, -1.02637265,
  -3.15291667,  1.10184387,  2.8076968 , -1.0241452 ,  2.69216698,
  0.89416752,  2.28367098,  1.47246822,  2.78310287,  0.93101876,
  2.76915438, -0.77388561, -2.54255126, -1.41269836, -3.00214933,
  1.06695433,  2.71650444,  0.86597562, -2.83013949, -1.49316564,
  2.63041435,  0.81215339,  2.45260598,  1.86965351,  2.70166038,
  0.68190887, -2.86981964, -0.59303826,  2.24459054,  1.36333521,
  -2.53227591, -1.92458511,  3.21477448, -1.11780203, -3.34038156,
  1.13633162, -2.76253342,  0.98595091, -3.12768735,  3.09785122,
  -3.43203611, -3.25815957,  2.85122844, -3.42957468,  2.68677066,
  2.69927718,  2.61706737,  2.79679581, 2.5507754 , -2.88574274,
  -3.2704754 ,  2.85832237, -3.56763697, 2.74649111,  3.07527155,
  2.55249089, -3.14610323,  2.50847856, -3.26724505,  3.23054979,
  -3.46054412, -2.77690669
};

float biases36[] = {
  -2.55921718, -1.69086256,  2.47634487,  1.82180311, -2.08316291,
  -2.4165892 ,  2.493639  , -2.04521167, -1.63228879, -1.72278681,
  -2.22514737,  2.03803677,  1.84885031,  2.60205723,  2.45787213,
  2.23265118, -1.68351097, -1.91349287, -2.51902039, -1.50091747,
  1.71000834,  2.50347677, -2.29145082, -2.40523341, -1.91514174,
  2.18202601, -2.55481591,  2.06633425, -1.72137687,  1.78497812,
  -1.44567546, -1.97989914, -1.92851961,  1.86497225,  1.60544337,
  2.73618019, 0.22688646
};

float weights36[] = {
  2.19379968,  1.53111   ,  2.4586229 , -0.86681944, -2.43107604,
  -1.08726359, -2.62836355,  0.96399657,  1.82503029,  1.35852799,
  1.97846064,  1.61751798, -2.4252819 , -1.1217369 ,  2.13656979,
  0.85230119,  2.42097249, -0.91172926,  2.5023924 , -0.92977751,
  2.08198416,  1.19812081, -2.26146074, -0.66227577, -2.64326585,
  0.95560212, -1.98980846, -1.88454195, -2.37414885, -1.14077118,
  -2.51105634, -0.57472011,  2.46809511, -0.92431936,  2.7059575 ,
  -0.99185164,  2.23258977,  1.41709954,  2.29801156, -0.87042128,
  -2.49966361,  0.90628499, -2.00009606, -1.72825721,  2.27315349,
  1.0211    ,  2.27621505,  1.18779737,  2.71338175, -0.99692677,
  -2.12351962, -1.09410941,  2.00196315,  1.78633675, -2.87646242,
  1.01049769,  2.50203473, -0.93222875, -2.57392333,  0.92564811,
  2.23073063, -0.78638794,  2.4074082 ,  0.2338019 ,  2.70429267,
  -0.96101284, -2.65672637,  0.95214044, -2.41496219,  0.89922792,
  -2.29057204, -1.6750544,   2.41379505,  2.219498  , -2.81888254,
  -2.60973372,  1.93086229, 2.19283367, -2.77752979,  1.91183782,
  2.05127656,  2.26020156, 2.02511006, -2.20169552, -2.70175687,
  -2.69779631, -2.83647681, -2.32241056,  2.17498475,  2.32194834,
  2.27401997,  1.93006217, -2.28833542, -2.45753966,  2.0770186 ,
  2.41167315,  2.34044678, -2.22528999,  2.4284631 , -2.87318116,
  2.177489  , -2.18088999, 1.84624366,  2.02394674,  2.31759559,
  -2.51637181, -2.30572694, -2.97915698
};

#endif

Eloquent1::ML::Port::GaussianNB GNB_clf;
Eloquent2::ML::Port::RandomForest RF_clf;
Eloquent3::ML::Port::DecisionTree DT_clf;
Eloquent4::ML::Port::SEFR SEFR_clf;

Tinn tinn6;
Tinn tinn12;
Tinn tinn24;
Tinn tinn36;

#include <Wire.h>
#include <ds3231.h>


#include <SD.h>
#include <SPI.h>


const byte digiPot1cs      = 9;       // MCP42100 chip select pin
const byte digiPot2cs      = 8;       // MCP42100 chip select pin
const int  maxPositions    = 256;     // wiper can move from 0 to 255 = 256 positions
const byte pot0            = 0x11;    // pot0 addr
const byte pot1            = 0x12;    // pot1 addr


File myFile;
const int ledPin = 12;   //ledPin
const int buttonPin1 = 2; //Button to perform interrupt
const int buttonPin2 = 3;
const int labelPin = 4;
const int window = 15;
int shock_enb = 0;
int relay1 = 6;
int relay2 = 7;
int trigger = 10;

int pinCS = 53; // Pin 10 on Arduino Uno
int SensorInput1 = A0;
//int SensorInput2 = A5;
//int SensorInput3 = A2;
//int SensorInput4 = A7;
int raw = 0;
int ref = 511;
int tol = 5;
int rollIndex = 0;

int indLed1 = 41;
int indLed2 = 39;
int indLed3 = 33;
int indLed4 = 35;
int indLedAll = 37;

int error = 0;
int wpos = 0;
int calCount = 0;

int ledToggle = LOW;//led state
int label;
int NN6Label;
int NN12Label;
int NN24Label;
int NN36Label;
int LGRLabel;
int GNBLabel;
int DTLabel;
int RFLabel;
int SEFRLabel;
int Thrsh1Label;
int Thrsh2Label;
int Thrsh3Label;


bool STARTLOG = false;
bool CLBRTD = false;
//bool CLBRTD_tot = false;
//bool CLBRTD_1 = false;
//bool CLBRTD_2 = false;
//bool CLBRTD_3 = false;
//bool CLBRTD_4 = false;

char fileName[25];//initializing filename char array

float Vin = 5.007;
float Vout = 0;
float X_in[2];
float prevDif_norm = 0;
float Vout_norm = 0;
float M = 25.10872808633989;
float B = 0.49999999999999994;


float buffer = 0;

float Vout1_total = 0;
//float Vout2_total = 0;
//float Vout3_total = 0;
//float Vout4_total = 0;

float Vout1_array[window];
//float Vout2_array[window];
//float Vout3_array[window];
//float Vout4_array[window];

float Vout1_avg = 0;
//float Vout2_avg = 0;
//float Vout3_avg = 0;
//float Vout4_avg = 0;


float Vlast1 = 0;
//float Vlast2 = 0;
//float Vlast3 = 0;
//float Vlast4 = 0;




float Vbaselines[4];
float offSet1 = 0;
//float offSet2 = 0;
//float offSet3 = 0;
//float offSet4 = 0;
float Vbase = 0;

float prevDif = 0;
float prevDif1_total = 0;
//float prevDif2_total = 0;
//float prevDif3_total = 0;
//float prevDif4_total = 0;

float prevDif1_array[window];
//float prevDif2_array[window];
//float prevDif3_array[window];
//float prevDif4_array[window];

float prevDif1_avg = 0;
//float prevDif2_avg = 0;
//float prevDif3_avg = 0;
//float prevDif4_avg = 0;
float thresh = 0.5;
//float thresh_rate = -0.05;
struct ts t;

//variables to keep track of the timing of recent interrupts
unsigned long button_time = 0;
unsigned long last_button_time = 0;
static long lastUpdate;



void setup() {

  pinMode(ledPin, OUTPUT);
  pinMode(indLed1, OUTPUT);
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  pinMode(trigger, OUTPUT);
  pinMode(indLed2, OUTPUT);
  pinMode(indLed3, OUTPUT);
  pinMode(indLed4, OUTPUT);
  pinMode(indLedAll, OUTPUT);
  pinMode(pinCS, OUTPUT);
  pinMode(digiPot1cs, OUTPUT);
  pinMode(digiPot2cs, OUTPUT);
  pinMode(labelPin, INPUT_PULLUP);
  pinMode(buttonPin1, INPUT_PULLUP);
  // pinMode(buttonPin2, INPUT_PULLUP);

  //BUilding NN and loading in bias and weights
  tinn6 = xtbuild(2, 6, 1);;
  for (int i = 0; i < tinn6.nb; i++)
    tinn6.b[i] = biases6[i];
  for (int i = 0; i < tinn6.nw; i++)
    tinn6.w[i] = weights6[i];

  //BUilding NN and loading in bias and weights
  tinn12 = xtbuild(2, 12, 1);;
  for (int i = 0; i < tinn12.nb; i++)
    tinn12.b[i] = biases12[i];
  for (int i = 0; i < tinn12.nw; i++)
    tinn12.w[i] = weights12[i];

  //BUilding NN and loading in bias and weights
  tinn24 = xtbuild(2, 24, 1);;
  for (int i = 0; i < tinn24.nb; i++)
    tinn24.b[i] = biases24[i];
  for (int i = 0; i < tinn24.nw; i++)
    tinn24.w[i] = weights24[i];

  //BUilding NN and loading in bias and weights
  tinn36 = xtbuild(2, 36, 1);;
  for (int i = 0; i < tinn36.nb; i++)
    tinn36.b[i] = biases36[i];
  for (int i = 0; i < tinn36.nw; i++)
    tinn36.w[i] = weights36[i];



  //intizializing prev_dif moving average array
  for (int j = 0; j < window; j++) {
    prevDif1_array[j] = 0;
    //prevDif2_array[j] = 0;
    //prevDif3_array[j] = 0;
    //prevDif4_array[j] = 0;
    Vout1_array[j] = 0;
    //Vout2_array[j] = 0;
    //Vout3_array[j] = 0;
    //Vout4_array[j] = 0;
  }



  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(buttonPin1), button1_ISR, CHANGE);

  Wire.begin();
  Wire.setClock(400000);
  DS3231_init(DS3231_CONTROL_INTCN);


  // SD Card Initialization
  if (SD.begin())
  {
    Serial.println("SD card is ready to use.");
  } else
  {
    Serial.println("SD card initialization failed");
    return;
  }
  DS3231_get(&t);

  //!!!!!!!!!!!!!!!!!!!!!!!!!Change Directory Every Day of Testing!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  snprintf(fileName, sizeof(fileName), "JMLD4/%i%i%i%i.csv", t.mday, t.hour, t.min, t.sec);
  Serial.println(fileName);
  // Create/Open file
  myFile = SD.open(fileName, FILE_WRITE);

  // if the file opened okay, write to it:
  
  if (myFile) {
    Serial.println("Writing data header to file...");
    // Write to file
    myFile.print("Time");
    myFile.print(",");
    myFile.print("PanelLabel");
    myFile.print(",");
    myFile.print("Voltage1_raw");
    myFile.print(",");
    myFile.print("Voltage1_avg");
    myFile.print(",");
    myFile.print("Prev_dif1_avg");
    myFile.print(",");
    myFile.print("NN6Label1");
    myFile.print(",");
    myFile.print("NN12Label1");
    myFile.print(",");
    myFile.print("NN24Label1");
    myFile.print(",");
    myFile.print("NN36Label1");
    myFile.print(",");
    myFile.print("LGRLabel1");
    myFile.print(",");
    myFile.print("GNBLabel1");
    myFile.print(",");
    myFile.print("DTLabel1");
    myFile.print(",");
    myFile.print("RFLabel1");
    myFile.print(",");
    myFile.print("SEFRLabel1");
    myFile.print(",");
    myFile.print("Thresh1Label1");
    myFile.print(","); 
    myFile.print("Thresh2Label1");
    myFile.print(",");
    myFile.print("Thresh3Label1");
    myFile.print(",");         
    myFile.close(); // close the file
    Serial.println("Done.");
  }
  // if the file didn't open, print an error:
  else {
    Serial.println("error opening file");

  }
  setPotWiper(pot0, 126, digiPot1cs);
  setPotWiper(pot1, 126, digiPot1cs);
  setPotWiper(pot0, 126, digiPot2cs);
  setPotWiper(pot1, 126, digiPot2cs);
}

void loop() {
   while (!CLBRTD) {
    raw = analogRead(SensorInput1);
    if (raw) {
      error = ref - raw;
      if (error < -1 * tol || error > tol) {
        wpos = wpos + 0.5 * 256 * error / 1024;
        wpos = constrain(wpos, 0, 255);
        Serial.print("Error Out 1: ");
        Serial.print(error);
        Serial.print(" Wiper position: ");
        Serial.println(wpos);
        setPotWiper(pot0, wpos, digiPot1cs);
      }
      else {
        Serial.println("Calibration 1 Complete");
        myFile = SD.open(fileName, FILE_WRITE);
        myFile.print(",");
        myFile.println(wpos);
        myFile.close(); // close the file
        CLBRTD = true;
        Vbase = 0;
        for (int i = 0; i < window; i++) {
          raw = analogRead(SensorInput1);
          buffer = raw * Vin;
          Vbase = Vbase + (buffer) / 1024.0;
          delay(1000);
        } 
        Vbaselines[0] = Vbase / window;
        Serial.print("Baseline1 Voltage is ");
        Serial.println(Vbaselines[0]);
        offSet1 = 0.8189980674912952 - Vbaselines[0]/3;
      }
        delay(1000);
      }
    }
   
  




  if (STARTLOG) {
    //Serial.println("logginData");
    if (millis() - lastUpdate > 1000) {

      lastUpdate = millis();
      myFile = SD.open(fileName, FILE_WRITE);

      raw = analogRead(SensorInput1);
      buffer = raw * Vin;
      //normalize and offset by the diference between training avg and baseline[0]
      Vout = (buffer) / 1024.0;
      Serial.print(Vout);
      Serial.print(",");
      label = digitalRead(labelPin);

       if (label == 0){
              digitalWrite(indLedAll,HIGH);
            }
            else{
              digitalWrite(indLedAll,LOW);
            }
      

      myFile.print(lastUpdate / 1000.);
      myFile.print(",");
      myFile.print(label);
      myFile.print(",");
      myFile.print(Vout);
      myFile.print(",");

      Vout_norm = Vout/3 + offSet1;

      
      //Computing the rooling average of the voltage output
      Vout1_total = Vout1_total - Vout1_array[rollIndex];
      Vout1_array[rollIndex] = Vout_norm;
      Vout1_total = Vout1_total + Vout1_array[rollIndex];

      Vout1_avg = Vout1_total / window;
      
      Serial.print(Vout1_avg);
      Serial.print(",");


      prevDif_norm = (Vout1_avg - Vlast1)*M+B;
      Vlast1 = Vout1_avg;

      myFile.print(Vout1_avg);
      myFile.print(",");

      //Computing the change between avg output voltage
      prevDif1_total = prevDif1_total - prevDif1_array[rollIndex];
      prevDif1_array[rollIndex] = prevDif_norm;
      prevDif1_total = prevDif1_total + prevDif1_array[rollIndex];

      prevDif1_avg = prevDif1_total / window;

      Serial.println(prevDif1_avg);

      
      myFile.print(prevDif1_avg);
      myFile.print(",");

      //Model Inference
      //NN networks
      X_in[0] = Vout1_avg;
      X_in[1] = prevDif1_avg;
      
      float *out = xtpredict(tinn6,X_in);
      NN6Label = (out[0] >= thresh);

      out = xtpredict(tinn12,X_in);
      NN12Label = (out[0] >= thresh);

      out = xtpredict(tinn24,X_in);
      NN24Label = (out[0] >= thresh);

// if (NN24Label == 0 && shock_enb> 10){
      if (NN24Label == 0){
        digitalWrite(indLed2,HIGH);
        //digitalWrite(relay1, HIGH);
        //digitalWrite(relay2, HIGH);
        //delay(40);
        //digitalWrite(trigger,HIGH);
        //delay(15000);
        //digitalWrite(relay1, LOW);
        //digitalWrite(relay2, LOW);
        //digitalWrite(trigger,LOW);
        //shock_enb =0;
      }
      else{
        digitalWrite(indLed2,LOW);
      }

      
      out = xtpredict(tinn36,X_in);
      NN36Label = (out[0] >= thresh);

      float prob = exp(inter+X_in[0]*slope[0]+X_in[1]*slope[1])/(1+exp(inter+X_in[0]*slope[0]+X_in[1]*slope[1]));
      LGRLabel = (prob >= thresh);

      if (LGRLabel == 0){
        digitalWrite(indLed4,HIGH);
      }
      else{
        digitalWrite(indLed4,LOW);
      }

      GNBLabel = GNB_clf.predict(X_in);
    
      DTLabel = DT_clf.predict(X_in);

      RFLabel = RF_clf.predict(X_in);

      if (RFLabel == 0){
        digitalWrite(indLed3,HIGH);
      }
      else{
        digitalWrite(indLed3,LOW);
      }

      SEFRLabel = SEFR_clf.predict(X_in);

    
      Thrsh1Label = !(((X_in[0]>=V1_bl) && (X_in[0]<=V1_bh))&&((X_in[1]>=r1_bl) && (X_in[1]<=r1_bh)));
      
      Thrsh2Label = !(((X_in[0]>=V2_bl) && (X_in[0]<=V2_bh))&&((X_in[1]>=r2_bl) && (X_in[1]<=r2_bh)));

       if (Thrsh2Label == 0){
        digitalWrite(indLed1,HIGH);
      }
      else{
        digitalWrite(indLed1,LOW);
      }
      
      Thrsh3Label = !(((X_in[0]>=V3_bl) && (X_in[0]<=V3_bh))&&((X_in[1]>=r3_bl) && (X_in[1]<=r3_bh)));
      
      myFile.print(NN6Label);
      myFile.print(",");
      myFile.print(NN12Label);
      myFile.print(",");
      myFile.print(NN24Label);
      myFile.print(",");
      myFile.print(NN36Label);
      myFile.print(",");
      myFile.print(LGRLabel);
      myFile.print(",");
      myFile.print(GNBLabel);
      myFile.print(",");
      myFile.print(DTLabel);
      myFile.print(",");
      myFile.print(RFLabel);
      myFile.print(",");
      myFile.print(SEFRLabel);
      myFile.print(",");
      myFile.print(Thrsh1Label);
      myFile.print(",");
      myFile.print(Thrsh2Label);
      myFile.print(",");
      myFile.println(Thrsh3Label);
      //myFile.print(",");         
      myFile.close(); // close the file
      //myFile.println(",");
      shock_enb+= 1;
      rollIndex = rollIndex + 1;

      if (rollIndex >= window) {
        rollIndex = 0;
      }

    }
  }
  else{
    digitalWrite(indLed1,LOW);
    
  }
}



void setPotWiper(int addr, int pos, int csPin) {

  //pos = constrain(pos, 0, 255);            // limit wiper setting to range of 0 to 255

  digitalWrite(csPin, LOW);                // select chip
  SPI.transfer(addr);                      // configure target pot with wiper position
  SPI.transfer(pos);
  digitalWrite(csPin, HIGH);               // de-select chip

  /* // print pot resistance between wiper and B terminal
    long resistanceWB = ((rAB * pos) / maxPositions ) + rWiper;
    Serial.print("Wiper position: ");
    Serial.print(pos);
    Serial.print(" Resistance wiper to B terminal: ");
    Serial.print(resistanceWB);
    Serial.println(" ohms");
  */
}

void button1_ISR() {
  button_time = millis();
  //check to see if increment() was called in the last 250 milliseconds
  if (button_time - last_button_time > 250) {
    STARTLOG = !STARTLOG;
    ledToggle = !ledToggle;
    digitalWrite(ledPin, ledToggle);
    last_button_time = button_time;
  }

}
