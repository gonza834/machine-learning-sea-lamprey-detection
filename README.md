# Welcome
---
## Overview

This repository contains the code utilized in developing a ML-classifier model based detection algorithm for monitoring invasive sea lamprey contact with underwater surfaces. 


---

---
## Dependencies Breakdown
Below is a list of modules/additional software needed to run the notebooks in this repo:
 1. Jupyter 
 2. Numpy
 3. Pandas
 4. Orange <=3.3
 5. OS
 6. Matplotlib
 7. scikit-learn
 8. MicroMLGen
 9. Time
 10. Scipy
 11. PyMoo

 